import React from "react";
import {
  InputGroupText,
  InputGroup,
  InputGroupAddon,
  Input,
  Button
} from "reactstrap";
import "./sayanemu.css";
import { Link, Redirect } from "react-router-dom";

export default class SayaNemu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      imageUrl: "",
      description: "",
      redirect: false
      //tambah redirect agar bisa redirect ke path lain
    };
    this.AddNewProject = this.AddNewProject.bind(this);
    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeUrl = this.onChangeUrl.bind(this);
    this.onChangeDesc = this.onChangeDesc.bind(this);
  }
  onChangeDesc(event) {
    this.setState({
      description: event.target.value
    });
  }
  onChangeTitle(event) {
    this.setState({
      title: event.target.value
    });
  }
  onChangeUrl(event) {
    this.setState({
      imageUrl: event.target.value
    });
  }
  AddNewProject() {
    fetch("https://reduxblog.herokuapp.com/api/posts?key=ketemu", {
      method: "POST",
      mode: "cors",
      cache: "no-cache",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json"
      },
      redirect: "follow",
      referrer: "no-referrer",
      body: JSON.stringify({
        title: this.state.title,
        categories: this.state.imageUrl,
        content: this.state.description
      })
    }).then(() => {
      this.setState({
        redirect: true
      });
    });
  }
  render() {
    if (this.state.redirect) {
      return <Redirect to="/Ketemu"></Redirect>;
    }
    return (
      <div className="nemu">
        <InputGroup>
          <InputGroupText>Jenis Barang / Judul:</InputGroupText>
          <InputGroupAddon addonType="prepend"></InputGroupAddon>
          <Input onChange={this.onChangeTitle} />
        </InputGroup>
        <br />
        <InputGroup>
          <InputGroupText>Ciri-ciri / Ditemukan di:</InputGroupText>
          <InputGroupAddon addonType="prepend"></InputGroupAddon>
          <Input onChange={this.onChangeDesc} />
        </InputGroup>
        <br />
        <InputGroup>
          <InputGroupText>Link Foto Barang:</InputGroupText>
          <InputGroupAddon
            type="textarea"
            addonType="prepend"
          ></InputGroupAddon>
          <Input onChange={this.onChangeUrl} />
        </InputGroup>
        <Link to="/Ketemu">
          <Button onClick={this.AddNewProject} color="success">
            Post Temuan
          </Button>
        </Link>
      </div>
    );
  }
}
