import React from "react";
import { Link } from "react-router-dom";

function Landingpage() {
  return (
    <div className="body">
      <main>
        <div className="title">
          <h1>Nemu atau ilang?</h1>
        </div>
        <div className="button">
          <Link className="bton" to="/SayaNemu">
            <b>Saya Nemu Sesuatu</b>
          </Link>
          <Link className="bton" to="/SayaHilang">
            <b>Saya Hilang Sesuatu</b>
          </Link>
        </div>
      </main>
    </div>
  );
}

export default Landingpage;
