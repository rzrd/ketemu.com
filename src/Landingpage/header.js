import React from "react";
import { Link } from "react-router-dom";

function Header() {
  return (
    <div>
      <header>
        <div className="navflex">
          <div className="logo">
            <Link to="/">
              <b>ketemu.com</b>
            </Link>
          </div>
          <ul>
            <li>
              <Link to="/">
                <b>Home</b>
              </Link>
            </li>
            <li>
              <Link to="/Ketemu">
                <b>Daftar Temuan</b>
              </Link>
            </li>
            <li>
              <Link to="/Hilang">
                <b>Daftar Hilang</b>
              </Link>
            </li>
          </ul>
        </div>
      </header>
    </div>
  );
}

export default Header;
