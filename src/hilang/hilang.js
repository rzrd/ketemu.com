import React from "react";
import {
  Card,
  Button,
  CardImg,
  CardTitle,
  CardText,
  CardColumns,
  Modal,
  ModalBody,
  ModalFooter,
  CardBody,
  CardSubtitle
} from "reactstrap";

export default class Ketemu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      projectArray: [],
      title: "",
      itemId: null,
      isOpen: false,
      modal: false
    };

    this.btntoggle = this.btntoggle.bind(this);

    this.toggle = this.toggle.bind(this);

    this.deleteItem = this.deleteItem.bind(this);
  }

  componentDidMount() {
    fetch("https://reduxblog.herokuapp.com/api/posts?key=hilang")
      .then(response => response.json())
      .then(data => {
        this.setState(state => ({
          projectArray: [...state.projectArray, ...data]
        }));
      })
      .catch(err => {
        console.log(err);
      });
  }

  deleteItem() {
    fetch(`https://reduxblog.herokuapp.com/api/posts/${this.state.itemId}`, {
      method: "DELETE",
      mode: "cors",
      cache: "no-cache",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json"
      },
      redirect: "follow",
      referrer: "no-referrer"
    })
      .then(() => {
        this.setState({
          modal: false,
          title: "",
          itemId: ""
        });
        window.location.reload();
      })
      .catch(err => {
        console.log(err);
      });
  }
  btntoggle() {
    this.setState(prevState => ({ modal: !prevState.modal }));
  }
  toggle() {
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {
    return (
      <div>
        <h2 style={{ textAlign: "center" }}>Daftar Barang Hilang</h2>
        <CardColumns style={{ width: "90%", margin: "auto" }}>
          {this.state.projectArray.map(obj => {
            return (
              <Card key={obj.id} body>
                <CardImg
                  top
                  width="100%"
                  src={
                    obj.categories
                      ? obj.categories
                      : `https://ui-avatars.com/api/?name=${obj.title}`
                  }
                  alt={obj.title}
                />
                <CardBody>
                  <h2>
                    <CardTitle>{obj.title}</CardTitle>
                  </h2>
                  <b>
                    <CardSubtitle>Deskripsi/Hilang di:</CardSubtitle>
                  </b>
                  <CardText>{obj.content}</CardText>
                  <Button
                    onClick={() =>
                      this.setState({
                        modal: true,
                        itemId: obj.id,
                        title: obj.title
                      })
                    }
                    color="danger"
                  >
                    Delete
                  </Button>
                </CardBody>
              </Card>
            );
          })}
        </CardColumns>

        <Modal isOpen={this.state.modal} toggle={this.btntoggle}>
          <ModalBody>Are you sure want to delete {this.state.title}</ModalBody>
          <ModalFooter>
            <Button onClick={this.btntoggle}>No</Button>
            <Button onClick={this.deleteItem}>Yes, delete it</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
